/*
 * testDelete.cpp
 *
 *  Created on: 20/03/2014
 *      Author: leobrizolara
 */

#include "gtest/gtest.h"

#include <iostream>
#include <stdlib.h>

using namespace std;

enum State{created, deleted};

class DeleteMock{
public:
	State * state;
	DeleteMock(State * someState = NULL) : state(someState)
	{
		if(someState != NULL){
			*someState = created;
		}
	}

	~DeleteMock(){
		*state = deleted;
	}

	void setState(State * someState){
		this->state = someState;
	}


};

void runOutScope(State * state){
	DeleteMock mock = DeleteMock(state);
}

TEST(testDelete, runOutScope){
	State state = created;

	runOutScope(&state);

	EXPECT_EQ(deleted, state);
}

TEST(testDelete, deleteArray){
	State states[] = {created, created, created};
	DeleteMock * mocks = new DeleteMock[3];

	for(int i =0; i < 3; ++i){
		mocks[i] = DeleteMock(&states[i]);
	}

	delete [] mocks;

	for(int i =0; i < 3; ++i){
		EXPECT_EQ(deleted, states[i]);
	}
}


TEST(testDelete, freeArray){
	State states[] = {created, created, created};
	DeleteMock * mocks = new DeleteMock[3];

	for(int i =0; i < 3; ++i){
		mocks[i] = DeleteMock(&states[i]);
	}

	delete [] mocks;

	for(int i =0; i < 3; ++i){
		EXPECT_EQ(deleted, states[i]);
	}
}

