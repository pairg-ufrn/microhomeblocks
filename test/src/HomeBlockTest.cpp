/*
 * HomeBlockTest.cpp
 *
 *  Created on: 31/03/2014
 *      Author: leobrizolara
 */

#include "gtest/gtest.h"
#include "HomeBlock.h"
#include "Service.h"

using namespace MicroHomeBlocks;

class HomeBlockTest : public ::testing::Test{
public:
	static const char * someServiceName;
	static const char * otherServiceName;
	static const char * notExistServiceName;
	HomeBlock block;

	Service someService;
	void SetUp(){
		block = HomeBlock();
		someService = Service(someServiceName);
	}
};

const char * HomeBlockTest::someServiceName = "someService";
const char * HomeBlockTest::otherServiceName = "otherService";
const char * HomeBlockTest::notExistServiceName = "thisServiceNotExist";

TEST_F(HomeBlockTest, constructor){
	EXPECT_EQ(0, block.getServices().count());
	EXPECT_FALSE(block.containsService(""));
	EXPECT_FALSE(block.containsService(someServiceName));
	EXPECT_LT(block.findService(""), 0);
	EXPECT_LT(block.findService(someServiceName), 0);
}

TEST_F(HomeBlockTest, addService){
	EXPECT_FALSE(block.containsService(someServiceName));
	EXPECT_FALSE(block.containsService(otherServiceName));
	block.addService(Service(someServiceName));
	block.addService(Service(otherServiceName));
	EXPECT_TRUE(block.containsService(someServiceName));
	EXPECT_TRUE(block.containsService(otherServiceName));
}

TEST_F(HomeBlockTest, addManyServices){
	const char * names[] = {"a","b","c","d","e","f","g","h","i"};
	unsigned int count =0;
	Service services[] = {
			Service(names[count++]),Service(names[count++]),Service(names[count++]),
			Service(names[count++]),Service(names[count++]),Service(names[count++]),
			Service(names[count++]),Service(names[count++]),Service(names[count++])};

	EXPECT_EQ(9,count);

	for(unsigned int i =0; i < count; ++i){
		block.addService(services[i]);
	}
	EXPECT_EQ(count, block.getServices().count());
	for(unsigned int i =0; i < count; ++i){
		EXPECT_TRUE(block.containsService(names[i]));
		EXPECT_STREQ(names[i], block.getServices().get(i).getName());
	}
}

TEST_F(HomeBlockTest, getService){
	block.addService(someService);
	EXPECT_EQ(someService, block.getService(someServiceName));
	EXPECT_EQ(Service::nullService(), block.getService(notExistServiceName));
}

TEST_F(HomeBlockTest, findService){
	block.addService(someService);
	EXPECT_EQ(block.getServices().find(someService), block.findService(someServiceName));
	EXPECT_EQ(0, block.findService(someServiceName));
	EXPECT_EQ(block.getServices().find(notExistServiceName), block.findService(notExistServiceName));
	//Deve retornar valor negativo
	EXPECT_LT(block.findService(notExistServiceName),0);
}

TEST_F(HomeBlockTest, equals){
	HomeBlock someBlock1, someBlock2;
	EXPECT_EQ(someBlock1, someBlock2);

	someBlock1.addService(Service());
	someBlock2.addService(Service());
	EXPECT_EQ(someBlock1, someBlock2);

	HomeBlock otherBlock1, otherBlock2;
	otherBlock1.addService(Service("someService"));
	otherBlock2.addService(Service("someService"));
	EXPECT_EQ(otherBlock1, otherBlock2);
	EXPECT_NE(otherBlock1, someBlock1);
	EXPECT_NE(someBlock2, otherBlock2);
	EXPECT_NE(otherBlock1, someBlock2);
	EXPECT_NE(someBlock1, otherBlock2);


}


