/*
 * OperationTypeTest.cpp
 *
 *  Created on: 18/03/2014
 *      Author: leobrizolara
 */

#include "gtest/gtest.h"
#include "OperationType.h"
using namespace MicroHomeBlocks;

TEST(OperationTypeTest, differentValues){
	EXPECT_EQ(0, OperationType::NONE);
	EXPECT_NE((int)OperationType::NONE, (int)OperationType::READ);
	EXPECT_NE((int)OperationType::NONE, (int)OperationType::WRITE);
	EXPECT_NE((int)OperationType::NONE, (int)OperationType::EXECUTE);
	EXPECT_NE((int)OperationType::NONE, (int)OperationType::DELETE);
	EXPECT_NE((int)OperationType::READ, (int)OperationType::WRITE);
	EXPECT_NE((int)OperationType::READ, (int)OperationType::EXECUTE);
	EXPECT_NE((int)OperationType::READ, (int)OperationType::DELETE);
	EXPECT_NE((int)OperationType::WRITE, (int)OperationType::EXECUTE);
	EXPECT_NE((int)OperationType::WRITE, (int)OperationType::DELETE);
	EXPECT_NE((int)OperationType::EXECUTE, (int)OperationType::DELETE);
}


//TEST(OperationTypeTest, contains){
//	unsigned int readWrite = OperationType::READ | OperationType::WRITE;
////	unsigned int readExec = OperationType::READ | OperationType::EXECUTE;
////	unsigned int readDelete = OperationType::READ | OperationType::DELETE;
////	unsigned int WriteExecDelete = OperationType::EXECUTE | OperationType::WRITE | OperationType::DELETE;
////	unsigned int all = OperationType::READ | OperationType::WRITE | OperationType::EXECUTE | OperationType::DELETE;
////	unsigned int none = OperationType::NONE;
//
//	EXPECT_TRUE(OperationType::);
//}




