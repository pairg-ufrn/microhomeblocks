/*
 * ServiceTest.cpp
 *
 *  Created on: 18/03/2014
 *      Author: leobrizolara
 */

#include "gtest/gtest.h"
#include "Service.h"
#include "ServiceDefinition.h"

using namespace MicroHomeBlocks;

class ServiceTest : public ::testing::Test{
protected:
	Service service;
public:
	void SetUp(){
		service = Service();
	}
};

TEST_F(ServiceTest, defaultValues){
	EXPECT_EQ(NULL, service.getCallback());
	EXPECT_STREQ("", service.getName());
}

void callbackSumOne(void * obj, const Request & req, Response & resp){
	int * ptr = (int *)obj;
	*ptr = *ptr + 1;
}
void emptyCallback(void * obj, const Request & req, Response & resp){
}

TEST_F(ServiceTest, constructor){
	ServiceCallback callback = callbackSumOne;
	const char * servName = "service";
	service = Service(servName, callback);
	EXPECT_EQ(callback, service.getCallback());
	EXPECT_STREQ(servName, service.getName());
}

TEST_F(ServiceTest, setName){
	const char * servName = "service";

	EXPECT_STREQ("", service.getName());
	service.setName(servName);
	EXPECT_STREQ("service", service.getName());
	service.setName("");
	EXPECT_STREQ("", service.getName());
}

TEST_F(ServiceTest, equals){
	const char * servName = "service";
	Service defaultService = Service();
	Service namedService = Service(servName);
	Service completeService = Service(servName, callbackSumOne);
	Service otherService = Service(servName, emptyCallback);
	Service moreService = Service("name", emptyCallback);

	EXPECT_TRUE(defaultService == Service());
	EXPECT_TRUE(Service::nullService() == defaultService);
	EXPECT_TRUE(Service(servName) == Service("service"));
	EXPECT_TRUE(namedService == Service("service"));
	EXPECT_TRUE(Service("service", callbackSumOne) == completeService);
	EXPECT_TRUE(completeService == completeService);

	EXPECT_FALSE(completeService == defaultService);
	EXPECT_FALSE(completeService == otherService);
	EXPECT_FALSE(completeService == moreService);
	EXPECT_FALSE(moreService == otherService);
	EXPECT_FALSE(defaultService == namedService);
}

TEST_F(ServiceTest, execute){
	ServiceCallback callback = callbackSumOne;
	const char * servName = "service";

	Request req = Request();
	Response resp = Response();

	int number = 0;

	service.execute(&number,req,resp);
	EXPECT_EQ(0,number);

	service = Service(servName, callback);
	service.execute(&number,req, resp);
	EXPECT_EQ(1,number);

	service = Service("", NULL);
	service.execute(&number,req, resp);
	EXPECT_EQ(1,number);
}

