/*
 * RequestTest.cpp
 *
 *  Created on: 08/04/2014
 *      Author: leobrizolara
 */

#include "gtest/gtest.h"
#include "Request.h"

using namespace MicroHomeBlocks;

class RequestTest : public testing::Test {
public:
	Request request;

	void SetUp(){
		request = Request();
	}
};

TEST_F(RequestTest, constructor){
	EXPECT_EQ(0, request.getParameters().count());
}



