/*
 * lampTest.cpp
 *
 *  Created on: 13/03/2014
 *      Author: leobrizolara
 */

#include "gtest/gtest.h"

#include "examples/Lamp/Lamp.h"

#ifdef ARDUINO
#include "ArduinoMock.h"
using MicroHomeBlocks::ArduinoMock;
#endif

#define LAMP_PIN 2

class LampTest : public testing::Test {
 public:
	Lamp someLamp;

	LampTest() : someLamp(LAMP_PIN)
	{

	}

	#ifdef ARDUINO
		ArduinoMock * arduino;

		virtual void SetUp() {
			arduino = ArduinoMock::instance();
		}

		virtual void TearDown(){
			ArduinoMock::setInstance(ArduinoMock());
		}
	#endif
};

TEST_F(LampTest, constructor){
	EXPECT_EQ(LAMP_PIN, someLamp.getPin());
}

#ifdef ARDUINO

TEST_F(LampTest, isOn){
	arduino->setPinValue(LAMP_PIN, HIGH);
	EXPECT_EQ(true, someLamp.isOn());
	arduino->setPinValue(LAMP_PIN, LOW);
	EXPECT_EQ(false, someLamp.isOn());
}

TEST_F(LampTest, changePower){
	arduino->setPinValue(LAMP_PIN, HIGH);
	EXPECT_EQ(true, someLamp.isOn());
	arduino->setPinValue(LAMP_PIN, LOW);
	EXPECT_EQ(false, someLamp.isOn());
}

#endif

