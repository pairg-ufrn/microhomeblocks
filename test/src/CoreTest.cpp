/*
 * CoreTest.cpp
 *
 *  Created on: 18/03/2014
 *      Author: leobrizolara
 */

#include "gtest/gtest.h"

#include "Core.h"
#include "BlockRegister.h"
#include "Service.h"


#include "Request.h"
#include "Response.h"

using namespace MicroHomeBlocks;

class MockRegister : public BlockRegister{
public:
	static const char * serviceTestName;
	static const char * serviceSumOneName;

	ServiceDeclaration(serviceTest,
			void * object, const Request & request, Response & response)
	{

	}
	ServiceDeclaration(serviceSumOne,
			void * object, const Request & request, Response & response)
	{
		if(object != NULL){
			int * num = (int *)object;
			*num = *num + 1;
		}
	}
	void registerBlockStructure(Core & core){
		core.registerService(this, serviceTestName, serviceTest);
		core.registerService(this, serviceSumOneName, serviceSumOne);
	}
};

const char * MockRegister::serviceTestName = "serviceTest";
const char * MockRegister::serviceSumOneName = "SumOne";

class CoreTest : public ::testing::Test{
protected:
	Core core;
	MockRegister controller;
public:
	void SetUp(){
		core = Core();
		controller = MockRegister();
	}
};

//namespace MicroHomeBlocks{
//	//Mockup
//	class Service{
//	public:
//		void execute(void * object, const Request & request, Response & response){
//
//		}
//	};
//}

TEST_F(CoreTest, registerController){
	EXPECT_FALSE(core.containsBlockStructure(&controller));
	controller.registerBlockStructure(core);
	EXPECT_TRUE(core.containsBlockStructure(&controller));
	HomeBlock & blockStructure = core.getBlockStructure(&controller);
	EXPECT_TRUE(blockStructure.containsService(MockRegister::serviceSumOneName));
	EXPECT_TRUE(blockStructure.containsService(MockRegister::serviceTestName));
}

TEST_F(CoreTest, registerServiceExternally){
	const char * serviceName = "service";
	EXPECT_FALSE(core.containsBlockStructure(&controller));
	core.registerService(&controller, serviceName, &MockRegister::serviceTest);
	ASSERT_TRUE(core.containsBlockStructure(&controller));
	EXPECT_TRUE(core.getBlockStructure(&controller).containsService(serviceName));
}

TEST_F(CoreTest, getAndExecuteService){
	const char * serviceName = "sumOne";
	core.registerService(&controller, serviceName, &MockRegister::serviceSumOne);

	Request req = Request();
	Response resp = Response();
	int value = 0;
	core.getBlockStructure(&controller).getService(serviceName).execute((void *)&value, req, resp);

	EXPECT_EQ(1, value);
}

/*******************************Executar Serviço**************************************/

TEST_F(CoreTest, registerNetwork){
	//core.addNetwork()
}


