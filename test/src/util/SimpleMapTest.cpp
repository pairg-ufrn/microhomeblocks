/*
 * SimpleMapTest.cpp
 *
 *  Created on: 01/04/2014
 *      Author: leobrizolara
 */

#include "gtest/gtest.h"
#include "util/SimpleMap.hpp"
#include "HomeBlock.h"

using namespace MicroHomeBlocks;

#include <stdlib.h> //rand
#include <string> //std::string
#include <sstream> //std::string

using namespace std;

class SimpleMapTest : public ::testing::Test{
public:
	SimpleMapTest(){
		srand(time(NULL));
	}
};

TEST_F(SimpleMapTest, constructor){
	SimpleMap<int, int> intToInt = SimpleMap<int, int>();

	EXPECT_EQ(0, intToInt.count());
	EXPECT_FALSE(intToInt.contains(0));
	EXPECT_FALSE(intToInt.contains(rand()));
}

TEST_F(SimpleMapTest, putInts){
	SimpleMap<int, int> intToInt = SimpleMap<int, int>();

	int someKey = rand(), someValue = rand();

	intToInt.put(someKey,someValue);
	EXPECT_EQ(someValue, intToInt.get(someKey));
	intToInt.put(0, -1000);
	EXPECT_EQ(-1000, intToInt.get(0));
}

void addManyInts(int numberValues, int * values, SimpleMap<int,int> & intToInt, int maxValue=0){
	for(int i=0; i < numberValues; ++i){
		if(maxValue <= 0){
			values[i] = rand();
		}
		else{
			values[i] = rand() % maxValue;
		}
		intToInt.put(i, values[i]);
	}
}


#define MANYCOUNT 20
TEST_F(SimpleMapTest, putManyInts){
	SimpleMap<int, int> intToInt = SimpleMap<int, int>();

	int values[MANYCOUNT];

	addManyInts(MANYCOUNT, values,intToInt);

	for(int i=0; i < MANYCOUNT; ++i){
		EXPECT_EQ(values[i], intToInt.get(i));
	}
}


TEST_F(SimpleMapTest, putHomeBlocks){
	SimpleMap<unsigned int, HomeBlock> uintToBlock = SimpleMap<unsigned int, HomeBlock>();

	EXPECT_EQ(0, uintToBlock.count());

	HomeBlock someBlock = HomeBlock(3);

	const char * someServiceNames[] = {"someService1","someService2","someService3"};
	someBlock.addService(Service(someServiceNames[0]));
	someBlock.addService(Service(someServiceNames[1]));
	someBlock.addService(Service(someServiceNames[2]));

	unsigned int someKey = rand();
	uintToBlock.put(someKey, someBlock);

	EXPECT_EQ(1, uintToBlock.count());
	EXPECT_TRUE(uintToBlock.get(someKey) == someBlock);
	EXPECT_EQ(someBlock, uintToBlock.get(someKey) );

	EXPECT_TRUE(uintToBlock.get(someKey).containsService(someServiceNames[0]));
	EXPECT_TRUE(uintToBlock.get(someKey).containsService(someServiceNames[1]));
	EXPECT_TRUE(uintToBlock.get(someKey).containsService(someServiceNames[2]));
}

TEST_F(SimpleMapTest, putManyHomeBlocks){
	SimpleMap<unsigned int, HomeBlock> uintToBlock = SimpleMap<unsigned int, HomeBlock>();
	EXPECT_EQ(0, uintToBlock.count());

	const int BLOCKS_NUM = 25;
	HomeBlock blocks[BLOCKS_NUM];
	string blocksServiceNames[BLOCKS_NUM];

	for(int i =0; i < BLOCKS_NUM; ++i){
		stringstream ss;
		ss << "Service" << i;
		blocksServiceNames[i] = ss.str();

		blocks[i].addService(Service(blocksServiceNames[i].c_str()));
		uintToBlock.put(i, blocks[i]);
	}

	EXPECT_EQ(BLOCKS_NUM, uintToBlock.count());
	for(int i =0; i < BLOCKS_NUM; ++i){
		EXPECT_EQ(blocks[i], uintToBlock.get(i));
	}


}

TEST_F(SimpleMapTest, putUpdateInts){
	SimpleMap<int, int> intToInt = SimpleMap<int, int>();

	int someKey = rand(), someValue = rand(), otherValue = rand(), anotherValue = rand();

	intToInt.put(someKey, someValue);
	EXPECT_EQ(1, intToInt.count());
	EXPECT_EQ(someValue, intToInt.get(someKey));

	intToInt.put(someKey, otherValue);
	EXPECT_EQ(1, intToInt.count());
	EXPECT_EQ(otherValue, intToInt.get(someKey));

	intToInt.put(someKey, anotherValue);
	EXPECT_EQ(1, intToInt.count());
	EXPECT_EQ(anotherValue, intToInt.get(someKey));
}

TEST_F(SimpleMapTest, putUpdateHomeBlocks){
	SimpleMap<unsigned int, HomeBlock> uintToBlock = SimpleMap<unsigned int, HomeBlock>();

	int blocksCount = 3;
	HomeBlock someBlocks[blocksCount];

	const char * someServiceNames[] = {"someService1","someService2","someService3"};

	for(int i=0; i < blocksCount; ++i){
		someBlocks[i].addService(Service(someServiceNames[i]));
	}

	unsigned int someKey=0;
	for(int i=0; i < blocksCount; ++i){
		//UpdateBlock
		uintToBlock.put(someKey, someBlocks[i]);
		EXPECT_EQ(1, uintToBlock.count());

		for(int j=0; j < blocksCount; ++j){
			HomeBlock & block = uintToBlock.get(someKey);
			const char * name = someServiceNames[j];
			if(i == j){
				EXPECT_TRUE(block.containsService(name));
			}
			else{
				EXPECT_FALSE(block.containsService(name));
			}
		}
	}
}

template <typename Key, typename Value>
void printMap(SimpleMap<Key, Value> & map, const char * mapName, unsigned int itemsByRow){

	cout<< mapName<< ": { ";
	for(unsigned int i =0; i < map.count(); ++i){
		const Key & key = map.getKeys().get(i);
		cout<< "("<<key<< ", " << map.get(key) << ")";
		if(i < map.count() - 1){
			cout<<", ";
		}
		if(itemsByRow >0 && (i+1)%itemsByRow == 0){
			//Quebra linha
			cout<<endl;
		}
	}
	cout<<"}"<<endl;

}
TEST_F(SimpleMapTest, removeInt){
	SimpleMap<int,int> intToInt;

	int values[MANYCOUNT];

	addManyInts(MANYCOUNT, values,intToInt, 100);

	printMap<int,int>(intToInt, "intToInt", 5);

	EXPECT_EQ(MANYCOUNT, intToInt.count());

	for(int i=0; i < MANYCOUNT; ++i){
		intToInt.remove(i);
		printMap<int,int>(intToInt, "intToInt", 5);
		EXPECT_EQ(MANYCOUNT - (i+1), intToInt.count());
		EXPECT_FALSE(intToInt.contains(i));
	}
}

//
//

TEST_F(SimpleMapTest, removeRandom){
	SimpleMap<int,int> intToInt;

	int values[MANYCOUNT];

	addManyInts(MANYCOUNT, values,intToInt);

	int keyToRemove = rand() % MANYCOUNT;

	intToInt.remove(keyToRemove);

	EXPECT_FALSE(intToInt.contains(keyToRemove));

	//Testa se valores correspondem ao esperado (para todas as chaves diferentes de keyToRemove)
	for(int i=0; i < MANYCOUNT -1; ++i){
		if(i < keyToRemove){
			EXPECT_EQ(values[i], intToInt.get(i));
		}
		else{
			EXPECT_EQ(values[i+1], intToInt.get(i+1));
		}
	}
}

TEST_F(SimpleMapTest, containsInt){
	SimpleMap<int,int> intToInt;

	int values[MANYCOUNT];

	addManyInts(MANYCOUNT, values,intToInt);

	for(int i=0; i < MANYCOUNT; ++i){
		EXPECT_TRUE(intToInt.contains(i));
	}
	for(int i=MANYCOUNT - 1; i >= 0; --i){
		intToInt.remove(i);
		EXPECT_FALSE(intToInt.contains(i));
	}
}
