/*
 * SimpleListTest.cpp
 *
 *  Created on: 21/03/2014
 *      Author: leobrizolara
 */

#include "gtest/gtest.h"
#include "util/SimpleList.hpp"
#include "Service.h"

#include <string>

using namespace MicroHomeBlocks;
using namespace std;

class SimpleListTest : public ::testing::Test{
};


TEST_F(SimpleListTest, constants){
	SimpleList<int> list = SimpleList<int>();
	//LIST_NOT_FOUND_INDEX deve ser um índice não válido (menor que 0)
	EXPECT_LT(LIST_NOT_FOUND_INDEX, 0);
}

TEST_F(SimpleListTest, defaultConstructor){
	SimpleList<int> list = SimpleList<int>();
	EXPECT_EQ(0, list.count());
}

TEST_F(SimpleListTest, addInteger){
	SimpleList<int> list = SimpleList<int>();
	list.add(1);
	list.add(2);
	EXPECT_EQ(2, list.count());
	EXPECT_TRUE(list.contains(1));
	EXPECT_TRUE(list.contains(2));
	list.add(1);
	list.add(2);
	EXPECT_EQ(4, list.count());
}
TEST_F(SimpleListTest, addService){
	SimpleList<Service> list = SimpleList<Service>();
	list.add(Service());
	list.add(Service("serviceName"));
	EXPECT_EQ(2, list.count());
	EXPECT_TRUE(list.contains(Service()));
	EXPECT_TRUE(list.contains(Service("serviceName")));
	list.add(Service());
	list.add(Service("serviceName"));
	EXPECT_EQ(4, list.count());
}
TEST_F(SimpleListTest, addManyService){
	SimpleList<Service> list = SimpleList<Service>();
	Service services[30];

	//FIXME: algum modo melhor de fazer isto??
	const char * serviceNames[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"
						   , "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"
						   , "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"};

	for(int i=0; i<30; ++i){
		services[i].setName(serviceNames[i]);
		list.add(services[i]);
	}

	EXPECT_EQ(30, list.count());

	for(int i=0; i<30; ++i){
		EXPECT_STREQ(serviceNames[i], list.get(i).getName());
	}
}
TEST_F(SimpleListTest, find){
	//FIXME: não copiar e colar!
	SimpleList<Service> list = SimpleList<Service>();
	Service services[30];

	//FIXME: algum modo melhor de fazer isto??
	const char * serviceNames[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"
						   , "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"
						   , "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"};

	for(int i=0; i<30; ++i){
		services[i].setName(serviceNames[i]);
		list.add(services[i]);
	}

	EXPECT_EQ(0, list.find(Service("1")));
	EXPECT_EQ(14, list.find(services[14]));
	EXPECT_EQ(29, list.find(Service("30")));
	//Não existentes
	EXPECT_GT(0, list.find(Service("3000")));
	EXPECT_GT(0, list.find(Service()));

}
TEST_F(SimpleListTest, contains){
	//FIXME: não copiar e colar!
	SimpleList<Service> list = SimpleList<Service>();
	Service services[30];

	//FIXME: algum modo melhor de fazer isto??
	const char * serviceNames[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"
						   , "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"
						   , "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"};

	for(int i=0; i<30; ++i){
		services[i].setName(serviceNames[i]);
		list.add(services[i]);
	}

	//Testando de trás pra frente
	for(int i = 29; i >= 0; --i){
		EXPECT_TRUE(list.contains(services[i]));
	}
	//Não existentes
	EXPECT_FALSE(list.contains(Service("3000")));
	EXPECT_FALSE(list.contains(Service()));

}
TEST_F(SimpleListTest, getService){
	SimpleList<Service> list = SimpleList<Service>();
	Service serv1 = Service();
	Service serv2 = Service("myService");
	Service serv3 = Service("moreOneService");
	list.add(serv1);
	list.add(serv2);
	list.add(serv3);
	EXPECT_EQ(serv2, list.get(1));
	EXPECT_EQ(serv1, list.get(0));
	EXPECT_EQ(serv3, list.get(2));
}
TEST_F(SimpleListTest, removeService){
	SimpleList<Service> list = SimpleList<Service>();
	Service serv1 = Service();
	Service serv2 = Service("myService");
	Service serv3 = Service("moreOneService");
	list.add(serv1);
	list.add(serv2);
	list.add(serv3);
	list.remove(serv2);
	//Testa ordem e tamanho apos remoção
	EXPECT_EQ(serv3, list.get(1));
	EXPECT_EQ(serv1, list.get(0));
	EXPECT_EQ(2, list.count());
	list.remove(serv1);
	//Testa ordem e tamanho apos remoção
	EXPECT_EQ(serv3, list.get(0));
	EXPECT_EQ(1, list.count());
	//Repetição proposital: remover um elemento não contido não deve alterar lista
	list.remove(serv2);
	list.remove(serv1);
	EXPECT_EQ(serv3, list.get(0));
	EXPECT_EQ(1, list.count());
	//remover ultimo elemento
	list.remove(serv3);
	EXPECT_EQ(0, list.count());
}

TEST_F(SimpleListTest, removeManyInts){
	SimpleList<int> list = SimpleList<int>();
	unsigned int number = 20;
	for(unsigned int i=0; i<number; ++i){
		list.add(i);
	}
	EXPECT_EQ(number, list.count());
	for(unsigned int i=number; i>0; --i){
		EXPECT_EQ(i-1, list.get(i-1));
	}
	for(unsigned int i=0; i<number; ++i){
		//Remove primeiro elemento
		list.removeAt(0);
		EXPECT_EQ(number - (i+1), list.count());
		EXPECT_FALSE(list.contains(i));
	}
}

typedef const char * cstr;

TEST_F(SimpleListTest, cstrListContains){
	SimpleList<cstr> list = SimpleList<cstr>();
	cstr someCstr = "anything";
	cstr otherCstr = "other";
	cstr sameCstr = "anything";
	string copyString = string(someCstr), sameString = string("anything");

	list.add(someCstr);
	list.add(otherCstr);
	//trivial
	EXPECT_TRUE(list.contains(someCstr));
	EXPECT_TRUE(list.contains(otherCstr));
	//copy
	EXPECT_TRUE(list.contains(sameCstr));
	//convert
	EXPECT_TRUE(list.contains(copyString.c_str()));
	EXPECT_TRUE(list.contains(sameString.c_str()));

}

TEST_F(SimpleListTest, cstrListFind){
	SimpleList<cstr> list = SimpleList<cstr>();
	cstr someCstr = "anything";
	cstr otherCstr = "other";
	cstr sameCstr = "same";
	list.add(someCstr);
	list.add(otherCstr);
	list.add(sameCstr);

	EXPECT_EQ(2, list.find("same"));
	EXPECT_EQ(0, list.find("anything"));
	EXPECT_EQ(1, list.find("other"));
	EXPECT_EQ(2, list.find(string("same").c_str()));
	EXPECT_EQ(0, list.find(string("anything").c_str()));
	EXPECT_EQ(1, list.find(string("other").c_str()));
}

TEST_F(SimpleListTest, notConstCstrListFind){
	SimpleList<char *> list = SimpleList<char *>();
	cstr someCstr = "anything";
	cstr otherCstr = "other";
	cstr sameCstr = "same";
	char * cstr = new char[3];
	cstr = (char *)"ab";
	list.add((char *)someCstr);
	list.add((char *)otherCstr);
	list.add((char *)sameCstr);
	list.add(cstr);


	EXPECT_EQ(2, list.find((char *)"same"));
	EXPECT_EQ(0, list.find((char *)"anything"));
	EXPECT_EQ(1, list.find((char *)"other"));
	EXPECT_EQ(3, list.find((char *)"ab"));
	EXPECT_EQ(0, list.find((char *)string("anything").c_str()));
	EXPECT_EQ(1, list.find((char *)string("other").c_str()));
	EXPECT_EQ(3, list.find((char *)string("ab").c_str()));
	EXPECT_EQ(2, list.find((char *)string("same").c_str()));
}



TEST(SimpleListDeathTest, getServiceDeathEmpty){
	::testing::FLAGS_gtest_death_test_style = "threadsafe";

	SimpleList<Service> list = SimpleList<Service>();
	//Testar: não é permitido obter um elemento que não exista
#ifndef NDEBUG //Macro que define se asserção será executada
	EXPECT_DEATH(list.get(0), ".*");
#endif
	list.add(Service());
	list.add(Service("otherService"));
	list.add(Service("moreOneService"));

#ifndef NDEBUG
	EXPECT_DEATH(list.get(3), ".*");
#endif
}
TEST(SimpleListDeathTest, getServiceDeath){
	::testing::FLAGS_gtest_death_test_style = "threadsafe";

	SimpleList<Service> list = SimpleList<Service>();

	list.add(Service());
	list.add(Service("otherService"));
	list.add(Service("moreOneService"));

#ifndef NDEBUG
	EXPECT_DEATH(list.get(3), ".*");
#endif
}
TEST(SimpleListDeathTest, getServiceDeathNegative){
	::testing::FLAGS_gtest_death_test_style = "threadsafe";

	SimpleList<Service> list = SimpleList<Service>();

	list.add(Service());
	list.add(Service("otherService"));
	list.add(Service("moreOneService"));

#ifndef NDEBUG
	EXPECT_DEATH(list.get(-1), ".*");
#endif
}


