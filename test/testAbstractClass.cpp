/*
 * testAbstractClass.cpp
 *
 *  Created on: 18/03/2014
 *      Author: leobrizolara
 */
#include "gtest/gtest.h"
#include <iostream>

class AbstractClass{
public:
	virtual ~AbstractClass(){}
	virtual void something() const = 0;
};
class SubClass : public AbstractClass{
public:
	void something() const{
		std::cout <<"something!"<<std::endl;
	}
};

class TestAbstractClass{
private:
	SubClass subClass;
public:
	TestAbstractClass() : subClass(SubClass())
	{}
	const AbstractClass & returnAbs();
};

const AbstractClass & TestAbstractClass::returnAbs(){
	return this->subClass;
}

TEST(AbstractClassTest, basicTest){
	TestAbstractClass absClass = TestAbstractClass();
	absClass.returnAbs().something();
}


