#include "gtest/gtest.h"
#include <iostream>

using namespace std;

// To use a test fixture, derive a class from testing::Test.
class FixtureTest : public testing::Test {
 protected:  // You should make the members protected s.t. they can be
             // accessed from sub-classes.
	int member;


  // virtual void SetUp() will be called before each test is run.  You
  // should define it if you need to initialize the varaibles.
  // Otherwise, this can be skipped.
  virtual void SetUp() {
	  cout << "SetUp!"<<endl;
	  member = 0;
  }

  // virtual void TearDown() will be called after each test is run.
  // You should define it if there is cleanup work to do.  Otherwise,
  // you don't have to provide it.
  //
   virtual void TearDown() {
	  cout << "TearDown!"<<endl;
   }

  // A helper function that some test uses.
  int helper() {
	  cout<< "helper!" << endl;
	  return 0;
  }

//  // A helper function for testing Queue::Map().
//  void MapTester(const Queue<int> * q) {
//    // Creates a new queue, where each element is twice as big as the
//    // corresponding one in q.
//    const Queue<int> * const new_q = q->Map(Double);
//
//    // Verifies that the new queue has the same size as q.
//    ASSERT_EQ(q->Size(), new_q->Size());
//
//    // Verifies the relationship between the elements of the two queues.
//    for ( const QueueNode<int> * n1 = q->Head(), * n2 = new_q->Head();
//          n1 != NULL; n1 = n1->next(), n2 = n2->next() ) {
//      EXPECT_EQ(2 * n1->element(), n2->element());
//    }
//
//    delete new_q;
//  }
};

// When you have a test fixture, you define a test using TEST_F
// instead of TEST.

TEST_F(FixtureTest, helperTest) {

	EXPECT_EQ(0,helper());
}

TEST_F(FixtureTest, memberFixture) {

	EXPECT_EQ(0, this->member);
}
