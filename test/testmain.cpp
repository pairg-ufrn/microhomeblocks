/*
 * TestMain.cpp
 *
 *  Created on: 02/03/2014
 *      Author: leobrizolara
 */

#include "gtest/gtest.h"

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}


