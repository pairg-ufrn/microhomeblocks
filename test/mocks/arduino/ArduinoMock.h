/*
 * ArduinoMock.h
 *
 *  Created on: 15/03/2014
 *      Author: leobrizolara
 */

#ifndef ARDUINOMOCK_H_
#define ARDUINOMOCK_H_

#include "ints.h"
#include "ArduinoFake.h"

namespace MicroHomeBlocks {

#ifndef NULL
#define NULL 0
#endif

#define STD_PINNUMBER 21

class ArduinoMock {
	static ArduinoMock * singleton;
	int pinCount;
	uint8_t * pinModes;
	int * pinValues;
public:
	static ArduinoMock * instance();
	static void setInstance(const ArduinoMock & arduino);

	ArduinoMock(uint8_t pinNumber = STD_PINNUMBER);
	virtual ~ArduinoMock();
	//Copy constructor
	ArduinoMock(const ArduinoMock& other);
	//Assignment operator
	ArduinoMock& operator = (const ArduinoMock& other);

	int getPinCount() const;
	uint8_t getPinMode(uint8_t pin) const;
	void setPinValue(uint8_t pin, int value);

	void pinMode(uint8_t pin, uint8_t mode);
	void digitalWrite(uint8_t pin, uint8_t value);
	int digitalRead(uint8_t pin) const;
	int analogRead(uint8_t pin) const;
//	void analogReference(uint8_t mode);
	void analogWrite(uint8_t pin, int value);
private:
	bool checkPin(uint8_t pin) const;
	void copy(const ArduinoMock & other);
};


} /* namespace MicroHomeBlocks */

#endif /* ARDUINOMOCK_H_ */
