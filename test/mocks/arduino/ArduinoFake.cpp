/*
 * ArduinoFake.cpp
 *
 *  Created on: 15/03/2014
 *      Author: leobrizolara
 */
#ifndef __cplusplus
#define __cplusplus
#endif

#include "ArduinoFake.h"
#include "ArduinoMock.h"

using MicroHomeBlocks::ArduinoMock;

void pinMode(uint8_t pin, uint8_t mode){
	ArduinoMock::instance()->pinMode(pin,mode);
}

void digitalWrite(uint8_t pin, uint8_t value){
	ArduinoMock::instance()->digitalWrite(pin,value);
}
int digitalRead(uint8_t pin){
	return ArduinoMock::instance()->digitalRead(pin);
}
int analogRead(uint8_t pin){
	return ArduinoMock::instance()->analogRead(pin);
}
void analogWrite(uint8_t pin, int value){
	ArduinoMock::instance()->analogWrite(pin,value);
}



