/*
 * ints.h
 *
 *  Created on: 16/03/2014
 *      Author: leobrizolara
 */

#ifndef INTS_H_
#define INTS_H_

#ifdef __cplusplus
extern "C" {
#endif


//Parece que stdint não define uint8_t e similares
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;


#ifdef __cplusplus
}
#endif



#endif /* INTS_H_ */
