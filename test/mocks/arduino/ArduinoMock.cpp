/*
 * ArduinoMock.cpp
 *
 *  Created on: 15/03/2014
 *      Author: leobrizolara
 */

#include "ArduinoMock.h"
#include <assert.h>

namespace MicroHomeBlocks {

#ifndef NULL
#define NULL 0
#endif


//ArduinoMock * ArduinoMock::singleton = NULL;

	ArduinoMock* MicroHomeBlocks::ArduinoMock::singleton = NULL;

	ArduinoMock* ArduinoMock::instance() {
		if(singleton == NULL){
			singleton = new ArduinoMock();
		}
		return singleton;
	}

	void ArduinoMock::setInstance(const ArduinoMock& arduino) {
		if(singleton != NULL){
			*singleton = arduino;
		}
		else{
			singleton = new ArduinoMock(arduino);
		}
	}

	ArduinoMock::ArduinoMock(const uint8_t pinNumber) : pinCount(pinNumber)
	{
		assert(pinNumber > 0);
		this->pinModes = new uint8_t[pinNumber];
		this->pinValues = new int[pinNumber];

		for(uint8_t pin=0; pin < pinNumber; ++pin){
			pinModes[pin] = INPUT;
			pinValues[pin] = LOW;
		}
	}

	ArduinoMock::~ArduinoMock() {
		if(pinModes != NULL){
			delete[] pinModes;
		}
	}

	ArduinoMock::ArduinoMock(const ArduinoMock& other)
	: pinCount(other.pinCount), pinModes(new uint8_t[other.pinCount])
	{
		copy(other);
	}

	ArduinoMock& ArduinoMock::operator =(const ArduinoMock& other) {
		copy(other);
		return *this;
	}

	int ArduinoMock::getPinCount() const {
		return pinCount;
	}

	void ArduinoMock::setPinValue(uint8_t pin, int value) {
		this->pinValues[pin] = value;
	}

	void ArduinoMock::copy(const ArduinoMock& other) {
		assert(pinCount == other.pinCount);

		for(int i=0; i < pinCount; ++i){
			this->pinModes[i] = other.pinModes[i];
		}
	}

	bool ArduinoMock::checkPin(uint8_t pin) const{
		return pin >= 0 && pin < this->pinCount;
	}

	void ArduinoMock::pinMode(uint8_t pin, uint8_t mode) {
		assert(checkPin(pin));
		this->pinModes[pin] = mode;
	}

	uint8_t ArduinoMock::getPinMode(uint8_t pin) const {
		assert(checkPin(pin));
		return this->pinModes[pin];
	}


	void ArduinoMock::digitalWrite(uint8_t pin, uint8_t value) {
		assert(checkPin(pin));
		assert(value == LOW || value == HIGH);
		//FIXME: ver qual a diferença de comportamento com os diferentes
		//modos do pino (INPUT, OUTPUT, INPUT_PULLUP)
		this->setPinValue(pin, value);
	}

	int ArduinoMock::digitalRead(uint8_t pin) const{
		assert(checkPin(pin));
		return this->pinValues[pin];
	}

	int ArduinoMock::analogRead(uint8_t pin) const{
		return 0;
	}

	void ArduinoMock::analogWrite(uint8_t pin, int value) {
	}
//	void ArduinoMock::analogReference(uint8_t mode) {
//	}



} /* namespace MicroHomeBlocks */

