/*
 * Arduino.h
 *
 *  Created on: 15/03/2014
 *      Author: leobrizolara
 */

#ifndef ARDUINO_H_
#define ARDUINO_H_

#include "ArduinoFake.h"


//Parece que stdint não define uint8_t e similares
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;


#endif /* ARDUINO_H_ */
