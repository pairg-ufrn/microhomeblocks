/*
 * ArduinoMockTest.cpp
 *
 *  Created on: 16/03/2014
 *      Author: leobrizolara
 */

#include "gtest/gtest.h"
#include "ArduinoMock.h"

using MicroHomeBlocks::ArduinoMock;

class ArduinoMockTest : public ::testing::Test {
 protected:
	ArduinoMock * arduino;
	virtual void SetUp() {
		arduino = ArduinoMock::instance();
	}

	virtual void TearDown(){
		ArduinoMock::setInstance(ArduinoMock());
	}
};

TEST_F(ArduinoMockTest, singletonInitialization){
	ArduinoMock * singleton = ArduinoMock::instance();
	EXPECT_TRUE(singleton != NULL);
}

TEST_F(ArduinoMockTest, pinNumberGreaterZero){
	EXPECT_GT(arduino->getPinCount(), 0);
}

TEST_F(ArduinoMockTest, pinModeDefault){
	for(uint8_t pin=0; pin < arduino->getPinCount(); ++pin){
		EXPECT_EQ(INPUT, arduino->getPinMode(pin));
	}
}

TEST_F(ArduinoMockTest, pinValueDefault){
	for(uint8_t pin=0; pin < arduino->getPinCount(); ++pin){
		EXPECT_EQ(LOW, arduino->digitalRead(pin));
	}
}

TEST_F(ArduinoMockTest, setReadPinValue){
	uint8_t pinNumbers[] = {2, 4, 8, 20, 5, 7};
	int pinValues[] = {LOW, HIGH, LOW, LOW, HIGH, HIGH};

	for(int i = 0; i < 6; ++i){
		arduino->setPinValue(pinNumbers[i], pinValues[i]);
		EXPECT_EQ(pinValues[i], arduino->digitalRead(pinNumbers[i]));
	}

}

