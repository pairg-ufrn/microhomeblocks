# README

MicroHomeBlocks é um framework de software gratuito e de código aberto, para Arduino e Raspberry Pi, resultante de um projeto de pesquisa realizado na Universidade Federal do Rio Grande do Norte (UFRN), Brasil. 
Saiba mais sobre o framework e sobre o projeto de pesquisa na [página do projeto](http://www.microhomeblocks.pairg.dimap.ufrn.br). 


## Documentação
Para gerar a documentação do código, execute o seguinte comando (na raiz do projeto):

    doxygen 

Ele exige que esteja instalado os programas doxygen, dot e graphviz.
