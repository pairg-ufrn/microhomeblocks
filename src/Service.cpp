/*
 * Service.cpp
 *
 *  Created on: 18/03/2014
 *      Author: leobrizolara
 */

#include "Service.h"
#include "null.h"
#include <string.h>

namespace MicroHomeBlocks {

const Service& Service::nullService() {
	static Service nullService = Service();

	return nullService;
}

Service::Service(const char* someName, ServiceCallback someCallback,
		unsigned int supportedOperations,
		const ParameterDefinition* parameters)
	: name(someName), callback(someCallback), operations(supportedOperations)
{
}

const char* Service::getName() const { return this->name;}
const ServiceCallback Service::getCallback() const { return this->callback;}
unsigned int Service::getOperations() const { return operations;}

void Service::setName(const char* name) { this->name = name;}
void Service::setCallback(ServiceCallback callback) { this->callback = callback;}
void Service::setOperations(unsigned int supportedOperations){ this->operations = supportedOperations;}

void Service::execute(void* object, const Request& request, Response& response) const {
	if(callback != NULL){
		this->callback(object, request, response);
	}
}


bool operator==(Service & serv1, Service & serv2){
	return ((const Service &) serv1 == (const Service &)serv2);
}
bool operator!=(Service & serv1, Service & serv2){
	return ((const Service &) serv1 != (const Service &)serv2);
}
bool operator==(const Service & serv1, const Service & serv2){
	return (&serv1 == &serv2) ||
				(serv1.operations == serv2.operations
				    && serv1.getCallback() == serv2.getCallback()
					&& strcmp(serv1.getName(), serv2.getName()) == 0);
}
bool operator!=(const Service & serv1, const Service & serv2){
	return !(serv1 == serv2);
}

} /* namespace MicroHomeBlocks */
