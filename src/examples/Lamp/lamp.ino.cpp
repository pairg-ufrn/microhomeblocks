/*
 * lamp.ino.cpp
 *
 *  Created on: 14/03/2014
 *      Author: leobrizolara
 */

#ifdef ARDUINO
#include "Arduino.h"
#endif

#include "MicroHomeBlocks.h"
#include "Network.h"
using namespace MicroHomeBlocks;

#define LAMP_PIN 3;

#ifdef ARDUINO
#include "networks/arduino/SerialNetwork.h"
Network network = SerialNetwork(9600);
#else
#include "networks/standard/StdNetwork.h"
#include "lamp.ino.h"
Network network = StdNetwork();
#endif

using MicroHomeBlocks::Core;
Core core = Core();

#include "Lamp.h"
#include "LampController.h"

LampController controller = LampController();
Lamp myLamp = Lamp(3);

void setup(){
	core.addNetwork(&network);
//	core.registerBlockStructure(&controller);
	controller.registerBlockStructure(core);
	core.addBlock(&controller, &myLamp,"");
}

void loop(){
	core.update();
}



