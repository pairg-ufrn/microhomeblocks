/*
 * Lamp.cpp
 *
 *  Created on: 02/03/2014
 *      Author: leobrizolara
 */

#include "Lamp.h"
//#include "Arduino.h"

#ifdef ARDUINO
	#include "Arduino.h"
#else
	#include <iostream>
#endif

int Lamp::getPin() const {
	return this->lampPin;
}

Lamp::Lamp(int pin) : lampPin(pin)
{
#ifdef ARDUINO
	pinMode(this->lampPin, OUTPUT);
#endif
}

void Lamp::setPin(const int pin) {
#ifdef ARDUINO
	pinMode(this->lampPin, INPUT);//retorna ao modo default
	pinMode(pin, OUTPUT);
#endif
	this->lampPin = pin;
}

void Lamp::changePower(bool toOn) {
#ifdef ARDUINO
	digitalWrite(this->lampPin, (toOn ? HIGH : LOW));
#else
	std::cout<<"digitalWrite at pin " << this->lampPin
			<< " to " << (toOn? "HIGH" : "LOW") << std::endl;
#endif
}

bool Lamp::isOn() const{
#ifdef ARDUINO
	return (digitalRead(lampPin) == HIGH);
#else
	std::cout<<"digitalRead at pin " << this->lampPin << std::endl;
	return false;
#endif
}
