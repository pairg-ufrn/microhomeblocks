/*
 * LampController.h
 *
 *  Created on: 02/03/2014
 *      Author: leobrizolara
 */

#ifndef LAMPCONTROLLER_H_
#define LAMPCONTROLLER_H_

#define USE_OO

#include "Lamp.h"

#include "MicroHomeBlocks.h"

using namespace MicroHomeBlocks;

// class LampController EXTENDS_IF_OO(public Controller){
class LampController : public BlockRegister{
private:
	static const char * changePowerArgumentName;
	static const char * changePowerServiceName;
	static const ParameterDefinition changePowerServiceParameters[];
public:
	void registerBlockStructure(Core & core);

	ServiceDeclaration(executeChangePower,
			void * lampPtr, const Request & request, Response & response);
//	static void executeChangePower(void * lampPtr, const Request & request, Response & response);
};

#endif /* LAMPCONTROLLER_H_ */
