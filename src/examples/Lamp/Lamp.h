/*
 * Lamp.h
 *
 *  Created on: 02/03/2014
 *      Author: leobrizolara
 */

#ifndef LAMP_H_
#define LAMP_H_

class Lamp {
private:
	int lampPin;
public:
	Lamp(int pin);

	void changePower(bool toOn);
	bool isOn() const;

	int getPin() const;
	//Talvez não deva ser permitido modificar o pino durante a execução
	void setPin(const int pin);
};

#endif /* LAMP_H_ */
