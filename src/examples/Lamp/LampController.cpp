/*
 * LampController.cpp
 *
 *  Created on: 02/03/2014
 *      Author: leobrizolara
 */

#include "LampController.h"
#include "ParameterType.h"
#include "OperationType.h"

#include "ServiceDefinition.h"
using namespace MicroHomeBlocks;

const char * LampController::changePowerArgumentName = "power";
const char * LampController::changePowerServiceName = "change-power";
const ParameterDefinition LampController::changePowerServiceParameters[] =
		{ParameterDefinition(changePowerArgumentName, BOOL_TYPE)};

void LampController::registerBlockStructure(Core& core) {
	core.registerService(this, changePowerServiceName, executeChangePower,
			OperationType::EXECUTE,
			changePowerServiceParameters);
}

ServiceImplementation(LampController, executeChangePower,
		void * lampPtr, const Request & request, Response & response)
{
	Lamp * lamp = (Lamp *) lampPtr;
	if(!request.containsParameter(changePowerArgumentName)){
		response.setResponseCode(ERROR_REQUIRED_PARAMETERS);
	}
	else{
		bool changePowerTo = request.getParameter(changePowerArgumentName)->toBool();
		lamp->changePower(changePowerTo);
	}

	return;
}
