#ifndef OPERATIOINTYPE__H
#define OPERATIOINTYPE__H

namespace MicroHomeBlocks{
class OperationType{
public:
	enum Type{
		NONE    = 0,
		READ    = 1,
		WRITE   = 2,
		EXECUTE = 4,
		DELETE  = 8};
	};

	/** Determines if an Operation contains some OperationType.
	 *  For example, execute this function in an operation 'op' compound by READ and WRITE
	 *  (READ | WRITE) will return true in contains(op, READ) and false in contains(op,EXECUTE)
	 *  @param operation some operation compound by OperationType values
	 *  @operationType some operationType
	 *  @return true if operation was compound by operationType and false otherwise.
	 * */
//	static bool contains(unsigned int operation, OperationType::Type operationType){
//		return (operation & operationType) == operationType;
//	}
//
//	static unsigned int compose(unsigned int operation, OperationType::Type operationType){
//		return (operation | operationType);
//	}
//
//	static unsigned int exclude(unsigned int operation, OperationType::Type operationType){
//		return operation & (~operationType);
//	}
}
#endif
