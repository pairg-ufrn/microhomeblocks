
#ifndef PARAMETERTYPE_H_
#define PARAMETERTYPE_H_

namespace MicroHomeBlocks {

enum ParameterType {BOOL_TYPE, INT_TYPE, STR_TYPE /* ... */};

} /* namespace MicroHomeBlocks */

#endif /* PARAMETERDEFINITION_H_ */
