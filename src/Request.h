/*
 * Request.h
 *
 *  Created on: 02/03/2014
 *      Author: leobrizolara
 */

#ifndef REQUEST_H_
#define REQUEST_H_
#include "Parameter.h"
#include "util/SimpleMap.hpp"

namespace MicroHomeBlocks {

typedef SimpleMap<const char *, Parameter> ParameterMap;

class Request {
private:
	ParameterMap parameters;
public:
	bool containsParameter(const char * parameterName) const;
	const Parameter * getParameter(const char * parameterName) const;
	ParameterMap & getParameters();
};

} /* namespace MicroHomeBlocks */

#endif /* REQUEST_H_ */
