/*
 * Request.cpp
 *
 *  Created on: 02/03/2014
 *      Author: leobrizolara
 */

#include "Request.h"

namespace MicroHomeBlocks {
	bool Request::containsParameter(const char* parameterName) const{
		return false;
	}

	const Parameter * Request::getParameter(const char* parameterName) const {
		return 0;
	}

ParameterMap& Request::getParameters() {
	return this->parameters;
}

} /* namespace MicroHomeBlocks */

