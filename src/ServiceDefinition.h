/*
 * ServiceDefinition.h
 *
 *  Created on: 19/03/2014
 *      Author: leobrizolara
 */

#ifndef SERVICEDEFINITION_H_
#define SERVICEDEFINITION_H_

#include "Request.h"
#include "Response.h"

#ifdef __cplusplus
extern "C"{
#endif

typedef void (*ServiceCallback) (void * ptr, const MicroHomeBlocks::Request & request, MicroHomeBlocks::Response & response );

#define ServiceDeclaration(methodName, objPointer, request, response) \
	static void methodName(objPointer, request, response)
#define ServiceImplementation(className,methodName, objPointer, request, response) \
	void className::methodName(objPointer, request, response)

#ifdef __cplusplus
}
#endif

#endif /* SERVICEDEFINITION_H_ */
