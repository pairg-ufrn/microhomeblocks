/*
 * ParameterDefinition.h
 *
 *  Created on: 02/03/2014
 *      Author: leobrizolara
 */

#ifndef PARAMETERDEFINITION_H_
#define PARAMETERDEFINITION_H_

#include "ParameterType.h"

namespace MicroHomeBlocks {

class ParameterDefinition {
private:
	const char * name;
	const ParameterType type;
public:
	ParameterDefinition(const char * str, ParameterType paramType)
		: name(str), type(paramType)
	{}

	const char * getName() const;
	const ParameterType getType() const;
};

} /* namespace MicroHomeBlocks */

#endif /* PARAMETERDEFINITION_H_ */
