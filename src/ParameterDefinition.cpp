/*
 * ParameterDefinition.cpp
 *
 *  Created on: 02/03/2014
 *      Author: leobrizolara
 */

#include "ParameterDefinition.h"

namespace MicroHomeBlocks {
	const char* ParameterDefinition::getName() const {
		return this->name;
	}

	const ParameterType ParameterDefinition::getType() const {
		return this->type;
	}
} /* namespace MicroHomeBlocks */

