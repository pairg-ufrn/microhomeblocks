/*
 * HomeBlock.cpp
 *
 *  Created on: 18/03/2014
 *      Author: leobrizolara
 */

#include "HomeBlock.h"
#include "Service.h"

#include <string.h>

namespace MicroHomeBlocks {

HomeBlock::HomeBlock(unsigned int serviceCount)
	: services(SimpleList<Service>(serviceCount))
{
}

bool HomeBlock::containsService(const char* serviceName) const {
	bool contains = false;

	if(serviceName != NULL){
		for(unsigned int i=0; i< services.count() && !contains; ++i){
			contains = (strcmp(services.get(i).getName(), serviceName) == 0);
		}
	}

	return contains;
}

const HomeBlock& HomeBlock::nullHomeBlock() {
	static HomeBlock homeBlock = HomeBlock();
	return homeBlock;
}

Service& HomeBlock::getService(const char* serviceName) const {
	int serviceIndex = findService(serviceName);
	if(serviceIndex != LIST_NOT_FOUND_INDEX){
		return this->services.get(serviceIndex);
	}
	else{
		return  (Service &)Service::nullService();
	}
}

/** Retorna o índice do serviço de nome 'serviceName', caso exista.
 *  @param serviceName é o nome do serviço procurado
 *  @return o índice do serviço com nome 'serviceName' ou um valor negativo, caso ele não exista.
 * */
int HomeBlock::findService(const char* serviceName) const {
	assert(serviceName != NULL);

	int serviceIndex = LIST_NOT_FOUND_INDEX;
//	for(int i=0; i < services.count() && serviceIndex == NOT_FOUND_INDEX ; ++i){
//		if(strcmp(services.get(i).getName(), serviceName) == 0){
//			serviceIndex = i;
//		}
//	}
	for(unsigned int i=0; i < services.count(); ++i){
		if(strcmp(services.get(i).getName(), serviceName) == 0){
			serviceIndex = i;
			break;
		}
	}
	return serviceIndex;
}

void HomeBlock::addService(const Service& service) {
	this->services.add(service);
}

SimpleList<Service>& HomeBlock::getServices(){
	return this->services;
}

bool HomeBlock::operator ==(HomeBlock& otherBlock) const{
	return this->operator ==((const HomeBlock &) otherBlock);
}

bool HomeBlock::operator !=(HomeBlock& otherBlock) const{
	return this->operator !=((const HomeBlock &) otherBlock);
}

bool HomeBlock::operator ==(const HomeBlock& otherBlock) const{
	return this->services == otherBlock.services;
}

bool HomeBlock::operator !=(const HomeBlock& otherBlock) const{
	return !(this->operator ==(otherBlock));
}

} /* namespace MicroHomeBlocks */

