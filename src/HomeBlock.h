/*
 * HomeBlock.h
 *
 *  Created on: 18/03/2014
 *      Author: leobrizolara
 */

#ifndef HOMEBLOCK_H_
#define HOMEBLOCK_H_

#include "util/SimpleList.hpp"

#include "Service.h"

namespace MicroHomeBlocks {
//class Service;

class HomeBlock {
public:
	const static HomeBlock & nullHomeBlock();
private:
	SimpleList<Service> services;
public:
	HomeBlock(unsigned int serviceCount = 0);
	bool containsService(const char * serviceName) const;
	int findService(const char * serviceName) const;
	Service & getService(const char * serviceName) const;
	void addService(const Service & service);
	SimpleList<Service> & getServices();

	bool operator==(HomeBlock & otherBlock) const ;
	bool operator!=(HomeBlock & otherBlock) const ;
	bool operator==(const HomeBlock & otherBlock) const;
	bool operator!=(const HomeBlock & otherBlock) const;
};

} /* namespace MicroHomeBlocks */

#endif /* HOMEBLOCK_H_ */
