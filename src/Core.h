/*
 * Core.h
 *
 *  Created on: 02/03/2014
 *      Author: leobrizolara
 */

#ifndef CORE_H_
#define CORE_H_

#include "BlockRegister.h"
#include "ParameterDefinition.h"
#include "OperationType.h"

#ifndef NULL
#define NULL 0
#endif

#include "HomeBlock.h"
#include "ServiceDefinition.h"
#include "util/SimpleMap.hpp"

namespace MicroHomeBlocks {

class HomeBlock;
class Network;

typedef SimpleList<Network *> NetworkList;

class Core {
private:
	SimpleMap<unsigned int, HomeBlock> blocks;
	NetworkList networks;
public:
	void registerService(
				unsigned int id,
				const char * serviceName,
				ServiceCallback callback,
				int supportedOperations = OperationType::EXECUTE,
				const ParameterDefinition * parameters = NULL);
	inline void registerService(
				void * id,
				const char * serviceName,
				ServiceCallback callback,
				int supportedOperations = OperationType::EXECUTE,
				const ParameterDefinition * parameters = NULL)
	{
		this->registerService((unsigned int)id, serviceName, callback
				, supportedOperations, parameters);
	}
	//TODO: registerConfig, registerStatus ...

	void addBlock(unsigned int structureId, void * blockPointer, const char * name = "");
	inline void addBlock(void * id, void * blockPointer, const char * name = "")
	{
		this->addBlock((unsigned int)id, blockPointer, name);
	}

	bool containsBlockStructure(unsigned int structureId) const;
	inline bool containsBlockStructure(void * id) const
	{
		return this->containsBlockStructure((unsigned int)id);
	}

	HomeBlock & getBlockStructure(unsigned int structureId) const;
	inline HomeBlock & getBlockStructure(void * id) const
	{
		return this->getBlockStructure((unsigned int)id);
	}

	void addNetwork(Network * network);
	void removeNetwork(Network * network);
	bool containsNetwork(Network * network) const;
	NetworkList & getNetworks();


	void update();
};

} /* namespace MicroHomeBlocks */

#endif /* CORE_H_ */
