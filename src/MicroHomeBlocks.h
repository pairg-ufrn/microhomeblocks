/*
 * microhomeblocks.h
 *
 *  Created on: 02/03/2014
 *      Author: leobrizolara
 */

#ifndef MICROHOMEBLOCKS_H_
#define MICROHOMEBLOCKS_H_

//Incluindo headers do MicroHomeBlocks
#include "Core.h"
#include "ServiceDefinition.h"

namespace MicroHomeBlocks{

#ifdef USE_OO
#	define EXTENDS_IF_OO(classes) \
	: classes
#else
#	define EXTENDS_IF_OO(classes)
#endif

}



#endif /* MICROHOMEBLOCKS_H_ */
