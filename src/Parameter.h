/*
 * Parameter.h
 *
 *  Created on: 02/03/2014
 *      Author: leobrizolara
 */

#ifndef PARAMETER_H_
#define PARAMETER_H_

namespace MicroHomeBlocks {

class Parameter {
public:
	bool toBool() const;
};

} /* namespace MicroHomeBlocks */

#endif /* PARAMETER_H_ */
