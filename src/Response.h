/*
 * Response.h
 *
 *  Created on: 02/03/2014
 *      Author: leobrizolara
 */

#ifndef RESPONSE_H_
#define RESPONSE_H_

namespace MicroHomeBlocks {

//FIXME: consertar tipos de respostas
enum ResponseCode {ERROR_REQUIRED_PARAMETERS};

class Response {
public:
public:
	void setResponseCode(ResponseCode code);
};

} /* namespace MicroHomeBlocks */

#endif /* RESPONSE_H_ */
