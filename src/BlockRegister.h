/*
 * Controller.h
 *
 *  Created on: 02/03/2014
 *      Author: leobrizolara
 */

#ifndef BLOCKREGISTER_H_
#define BLOCKREGISTER_H_



namespace MicroHomeBlocks {
class Core;
class BlockRegister {
public:
	virtual ~BlockRegister(){};
	virtual void registerBlockStructure(Core & core)=0;
};

} /* namespace MicroHomeBlocks */

#endif /* CONTROLLER_H_ */
