/*
 * Core.cpp
 *
 *  Created on: 02/03/2014
 *      Author: leobrizolara
 */

#include "Core.h"
#include "HomeBlock.h"
#include "BlockRegister.h"
#include "Service.h"
#include <assert.h>

namespace MicroHomeBlocks {


void Core::registerService(unsigned int structureId, const char* serviceName,
		ServiceCallback serviceCallback, int supportedOperations,
		const ParameterDefinition* parameters) {
	Service service = Service(serviceName, serviceCallback, supportedOperations, parameters);

	if(!blocks.contains(structureId)){
		blocks.put(structureId, HomeBlock());
	}

	blocks.get(structureId).addService(service);
}

void Core::addBlock(unsigned int structureId, void* blockPointer,
		const char* name) {
}

bool Core::containsBlockStructure(unsigned int structureId) const {
	return blocks.contains(structureId);
}

HomeBlock& Core::getBlockStructure(unsigned int structureId) const {
	assert(blocks.contains(structureId));
	return blocks.get(structureId);
}

void Core::update() {
}

void Core::addNetwork(Network* network) {
	if(network != NULL){
		this->networks.add(network);
	}
}

void Core::removeNetwork(Network* network) {
	this->networks.remove(network);
}

bool Core::containsNetwork(Network* network) const {
	return this->networks.contains(network);
}

NetworkList& Core::getNetworks() {
	return this->networks;
}

} /* namespace MicroHomeBlocks */

