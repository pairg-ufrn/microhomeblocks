/*
 * SerialNetwork.h
 *
 *  Created on: 16/03/2014
 *      Author: leobrizolara
 */

#ifndef SERIALNETWORK_H_
#define SERIALNETWORK_H_

#include <Network.h>

namespace MicroHomeBlocks {

class SerialNetwork: public Network {
public:
	SerialNetwork(int baudrate);
};

} /* namespace MicroHomeBlocks */

#endif /* SERIALNETWORK_H_ */
