/*
 * StdNetwork.h
 *
 *  Created on: 14/03/2014
 *      Author: leobrizolara
 */

#ifndef STDNETWORK_H_
#define STDNETWORK_H_

#include "Network.h"

namespace MicroHomeBlocks {

class StdNetwork : public Network{
public:
	void receiveRequest();
};

} /* namespace MicroHomeBlocks */

#endif /* STDNETWORK_H_ */
