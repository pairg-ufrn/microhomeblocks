/*
 * SimpleMap.hpp
 *
 *  Created on: 01/04/2014
 *      Author: leobrizolara
 */

#ifndef SIMPLEMAP_HPP_
#define SIMPLEMAP_HPP_

#include "SimpleList.hpp"
#include <assert.h>

namespace MicroHomeBlocks{

	template<typename Key, typename Value>
	class SimpleMap{
	private:
		SimpleList<Key> keys;
		SimpleList<Value> values;

	public:
		SimpleMap()
			: keys(SimpleList<Key>()), values(SimpleList<Value>())
		{}

		const SimpleList<Key> & getKeys() const;
		const SimpleList<Value> & getValues() const;

		Value & get(const Key & key) const;
		void put(const Key & key, const Value & value);
		void remove(const Key & key);
		unsigned int count() const;
		bool contains(const Key & key) const;
	};

	template<typename Key, typename Value>
	inline Value& SimpleMap<Key, Value>::get(const Key& key) const {
		assert(keys.contains(key));
		int index = keys.find(key);
		return values.get(index);
	}

	template<typename Key, typename Value>
	inline void SimpleMap<Key, Value>::put(const Key& key, const Value& value) {
		int index = keys.find(key);
		if(index < 0){
			this->keys.add(key);
			this->values.add(value);
		}
		else{//Key continua a mesma
			this->values.set(index, value);
		}
	}

	template<typename Key, typename Value>
	inline void SimpleMap<Key, Value>::remove(const Key& key) {
		int index = keys.find(key);
		if(index >= 0){
			keys.removeAt(index);
			values.removeAt(index);
		}
	}

	template<typename Key, typename Value>
	inline unsigned int SimpleMap<Key, Value>::count() const {
		return keys.count();
	}

	template<typename Key, typename Value>
	inline bool SimpleMap<Key, Value>::contains(const Key& key) const {
		return keys.contains(key);
	}

	template<typename Key, typename Value>
	inline const SimpleList<Key>& SimpleMap<Key, Value>::getKeys() const {
		return this->keys;
	}

	template<typename Key, typename Value>
	inline const SimpleList<Value>& SimpleMap<Key, Value>::getValues() const {
		return this->values;
	}

};
#endif /* SIMPLEMAP_HPP_ */
