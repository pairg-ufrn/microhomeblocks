/*
 * SimpleList.h
 *
 *  Created on: 20/03/2014
 *      Author: leobrizolara
 */

#ifndef SIMPLELIST_H_
#define SIMPLELIST_H_


#include <iostream>
using std::cout;
using std::endl;

#include <assert.h>
#include "null.h"
#include "equals.hpp"

namespace MicroHomeBlocks {

#define DEFAULT_LIST_SIZE 4
#define DEFAULT_GROW_RATE 3
#define LIST_NOT_FOUND_INDEX -1

template<typename T>
class SimpleList {
private:
	T * values;
	unsigned int capacity;
	unsigned int size;
	unsigned int growRate;
public:
	SimpleList(unsigned int capacity = DEFAULT_LIST_SIZE, unsigned int growRate = DEFAULT_GROW_RATE);
	SimpleList(const SimpleList<T> & other);
	virtual ~SimpleList();

	SimpleList<T>& operator= (const SimpleList<T> & other);

	unsigned int count() const { return this->size;}
	T& get(unsigned int index) const;
	void set(unsigned int index, const T& element);
	void add(const T& element);
	void remove(const T& element);
	void removeAt(unsigned int index);
	int find(const T& element, unsigned int startIndex =0) const;
	bool contains(const T& element) const;
private:
//	bool equals(const T& element, const T& other) const;
//	bool equals(const char * & element, const char *& other) const;
	void resize(unsigned int newCapacity);
	void copy(T * otherValues, unsigned int otherSize);
public:

	bool operator==(SimpleList<T> & otherList) const;
	bool operator!=(SimpleList<T> & otherList) const;
	bool operator==(const SimpleList<T> & otherList) const;
	bool operator!=(const SimpleList<T> & otherList) const;
};


template<typename T>
inline bool SimpleList<T>::operator==(SimpleList<T> & otherList) const{
	return this->operator ==((const SimpleList<T>&)otherList);
}

template<typename T>
inline bool SimpleList<T>::operator!=(SimpleList<T> & otherList) const{
	return this->operator !=((const SimpleList<T>&)otherList);
}


template<typename T>
inline
bool MicroHomeBlocks::SimpleList<T>::operator ==(const SimpleList<T>& otherList) const
{
	if(otherList.count() != this->count()){
		return false;
	}

	for(unsigned int i=0;i< this->count();++i){
		if(this->get(i) != otherList.get(i)){
			return false;
		}
	}
	return true;
}

template<typename T>
inline
bool MicroHomeBlocks::SimpleList<T>::operator !=(const SimpleList<T>& otherList) const {
	return !(this->operator ==(otherList));
}

template<typename T>
inline SimpleList<T>::SimpleList(unsigned int initialCapacity, unsigned int growRate)
: capacity(initialCapacity), size(0), growRate(growRate)
{
	assert(growRate >= 1);
	this->values = new T[initialCapacity];
}

template<typename T>
inline MicroHomeBlocks::SimpleList<T>::~SimpleList() {
	if(values != NULL){
		delete [] this->values;
	}
}

template<typename T>
inline SimpleList<T>::SimpleList(const SimpleList<T>& other)
	: values(new T[other.capacity]),
	  capacity(other.capacity),
	  size(other.size),
	  growRate(other.growRate)
{
	//TODO: modificar para copy-on-write
	copy(other.values, other.size);
}

template<typename T>
inline SimpleList<T>& SimpleList<T>::operator =(const SimpleList<T>& other) {
	if(this->values != NULL && this->capacity < other.capacity){
		delete [] this->values;
		this->values = new T[other.capacity];
		this->capacity = other.capacity;
	}
	copy(other.values, other.size);

	return *this;
}


#ifndef LIST_BOUND_CHECK
template<typename T>
inline T& MicroHomeBlocks::SimpleList<T>::get(unsigned int index) const {
	assert(index < size && index >= 0);
	return this->values[index];
}
#else

template<typename T>
inline T& MicroHomeBlocks::SimpleList<T>::get(unsigned int index) const {
	if(index < size && index >= 0){
		return this->values[index];
	}
	else{
		static T defaultValue;
		return defaultValue;
	}
}

#endif

template<typename T>
inline void MicroHomeBlocks::SimpleList<T>::add(const T& element) {
	if(size >= capacity){
		resize(capacity + growRate);
	}
	this->values[size] = element;
	++size;
}


template<typename T>
inline void SimpleList<T>::set(unsigned int index, const T& element) {
	assert(index < size);
	this->values[index] = element;
}

template<typename T>
inline void MicroHomeBlocks::SimpleList<T>::resize(unsigned int newCapacity) {
	T * currentValues = this->values;
	this->values = new T[newCapacity];
	this->capacity = newCapacity;
	copy(currentValues, (this->size < newCapacity ? this->size : newCapacity));
	delete [] currentValues;
}

template<typename T>
inline void MicroHomeBlocks::SimpleList<T>::remove(const T& element) {
	int index = find(element);
	if(index != LIST_NOT_FOUND_INDEX){
		this->removeAt((unsigned int)index);
	}
}

template<typename T>
inline void SimpleList<T>::removeAt(unsigned int index) {
	assert(index >=0 && index < size);
//	if(!(index >=0 && index < size)){
//		std::cerr << "Erro! Index é " << index <<" e size é " << size <<endl;
//		return;
//	}
	//se indice não é o último
	if(index < (size -1)){
		//Move elementos para a esquerda
		//FIXME: memmove pode ser propenso a erros!
		memmove(&values[index], &values[index+1], sizeof(T) * (size - index + 1));
	}
	--size;
}



template<typename T>
inline bool SimpleList<T>::contains(const T& element) const{
	return (find(element) != LIST_NOT_FOUND_INDEX);
}

template<typename T>
inline int MicroHomeBlocks::SimpleList<T>::find(const T& element,unsigned int startIndex) const{
	int index = LIST_NOT_FOUND_INDEX;

	for(unsigned int i=startIndex; i < size && index == LIST_NOT_FOUND_INDEX; ++i){
//		if(equals(values[i], element)){
		if(equals(element, this->values[i])){
//		if(values[i] == element){
			index = i;
		}
	}

	return index;
}

template<typename T>
inline void MicroHomeBlocks::SimpleList<T>::copy(T* otherValues, unsigned int otherSize) {
	/*FIXME: isso é ineficiente! Pois é chamado o operador de atribuição (abaixo).
	 * Se os tipos mantêm memória dinâmica alocada, isso pode gerar problemas...
	 * Talvez seja melhor utilizar malloc/realloc. Embora seja mais "perigoso"...*/
	for(unsigned int i=0; i < capacity && i < otherSize; ++i){
		values[i] = otherValues[i];
	}
	this->size = (otherSize <= capacity ? otherSize : capacity);
}


} /* namespace MicroHomeBlocks */

#endif /* SIMPLELIST_H_ */
