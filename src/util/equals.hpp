/*
 * equals.h
 *
 *  Created on: 09/04/2014
 *      Author: leobrizolara
 */

#ifndef EQUALS_H_
#define EQUALS_H_

//#ifdef __cplusplus
//extern "C" {
//#endif


#include <cstring>

template<typename T>
bool equals(const T & t1, const T & t2);
bool equals(char * t1, char * t2);
bool equals(const char * t1, const char * t2);


template<typename T>
inline bool equals(const T& t1, const T& t2) {
	return t1 == t2;
}

inline bool equals(char* t1, char* t2) {
	return strcmp(t1,t2) == 0;
}
inline bool equals(const char* t1, const char* t2) {
	return strcmp(t1,t2) == 0;
}

//#ifdef __cplusplus
//}
//#endif




#endif /* EQUALS_H_ */
