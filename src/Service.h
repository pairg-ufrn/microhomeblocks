/*
 * Service.h
 *
 *  Created on: 18/03/2014
 *      Author: leobrizolara
 */

#ifndef SERVICE_H_
#define SERVICE_H_

#include "null.h"
#include "ServiceDefinition.h"
#include "OperationType.h"
#include "ParameterDefinition.h"

namespace MicroHomeBlocks {

class Request;
class Response;

class Service {
private:
	const char * name;
	ServiceCallback callback;
	unsigned int operations;
public:
	static const Service & nullService();
public:
	Service(const char * name = "", ServiceCallback callback = NULL,
			unsigned int supportedOperations = OperationType::EXECUTE,
			const ParameterDefinition* parameters = NULL);

	const ServiceCallback getCallback() const;
	const char * getName() const;
	unsigned int getOperations() const;

	void setName(const char * name);
	void setCallback(ServiceCallback callback);
	/** Set the operations supported by this service.
	 * @param supportedOperations should by a combination of one or more OperationType values.
	 * 		Example: OperationType::EXECUTE | OperationType::READ
	 * @see OperationType
	 * */
	void setOperations(unsigned int supportedOperations);

	void execute(void * object, const Request & request, Response & response) const;

	friend bool operator==(Service & serv1, Service & serv2);
	friend bool operator!=(Service & serv1, Service & serv2);
	friend bool operator==(const Service & serv1, const Service & serv2);
	friend bool operator!=(const Service & serv1, const Service & serv2);
};


bool operator==(Service & serv1, Service & serv2);
bool operator!=(Service & serv1, Service & serv2);
bool operator==(const Service & serv1, const Service & serv2);
bool operator!=(const Service & serv1, const Service & serv2);

} /* namespace MicroHomeBlocks */

#endif /* SERVICE_H_ */
